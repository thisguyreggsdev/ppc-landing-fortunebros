export default function Example() {
  return (
    <div className="bg-white pb-16 pt-24 sm:pb-24 sm:pt-22 xl:pb-32">
      <div className="bg-gray-800 pb-20 sm:pb-24 xl:pb-0">
        <div className="mx-auto flex max-w-7xl flex-col items-center gap-x-8 gap-y-10 px-6 sm:gap-y-8 lg:px-8 xl:flex-row xl:items-stretch">
          <div className="-mt-8 w-full max-w-2xl xl:-mb-8 xl:w-96 xl:flex-none">
            <div className="relative aspect-[2/1] h-full md:-mx-8 xl:mx-0 xl:aspect-auto">
              <img
                className="absolute inset-0 h-100 w-full rounded-2xl bg-gray-800 object-cover shadow-2xl"
                src="https://images.squarespace-cdn.com/content/v1/5cd9d27f840b16669a850ca3/1597788905903-HHA44YY0AP7IW2DJVJCM/garin-barbers-den-barbershop-belmont.jpg"
                alt=""
              />
            </div>
          </div>
          <div className="w-full max-w-2xl xl:max-w-none xl:flex-auto xl:px-16 xl:py-24">
            <figure className="relative isolate pt-6 sm:pt-12">
              
              <blockquote className="text-1sm leading-8 text-white sm:leading-9">
                <p className="text-justify">
                 Fortune Bros Barber Shop offers a sophisticated and masculine grooming experience. We provide a welcoming and comfortable space for men to relax and enjoy high-quality grooming services. Our barbers are skilled in traditional barbering techniques, as well as knowledgeable about current styles and trends. We use top-notch products to ensure that our clients leave feeling and looking their best. Our range of services includes haircuts, beard trims, and hot towel treatments, all designed to cater to the specific grooming needs of men. Overall, we strive to provide a luxurious and masculine grooming experience that exceeds our clients' expectations.
                </p>
              </blockquote>
              <figcaption className="mt-8 text-base">
                <div className="font-semibold text-white">Alie Brooks</div>
                <div className="mt-1 text-white">Podcast Host/Owner</div>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
  )
}
