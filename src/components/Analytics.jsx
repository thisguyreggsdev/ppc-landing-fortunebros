import React from 'react';
import Laptop from '../assets/laptop.jpg';

const Analytics = () => {
  const handleClickScroll = () => {
    const formContainer = document.getElementById('form-container');
    if (formContainer) {
      formContainer.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div className='w-full bg-gray-800 py-16 px-4'>
      <div className='max-w-[1240px] mx-auto flex items-center justify-center md:flex-row flex-col'>
        <img className='w-80 h-150 mx-5' src={Laptop} alt='/' />
        <div className='flex flex-col justify-center'>
          <h1 className='md:text-4xl sm:text-3xl text-2xl font-bold py-2 text-white'>We'll take care of your grooming needs</h1>
          <p className='text-white font-lg-semibold'>
            With years of experience catering to the grooming needs of gentlemen, our traditional approach has evolved into a refined art form. Our team of skilled professionals is dedicated to providing exceptional service to all who seek to look dapper, sleek, and exude a sense of refined masculinity. From classic haircuts to modern grooming techniques, we strive to exceed your expectations and leave you feeling confident and empowered. Trust us to help you unleash your true potential and embrace your natural sense of style. 
          </p>
         <button className='text-white w-[200px] rounded-md font-medium my-6 mx-auto md:mx-0 py-3 border-2 border-white hover:bg-white hover:text-gray-900 transition duration-300 ease-in-out' onClick={handleClickScroll}>Inquire Now</button>
        </div>
      </div>
    </div>
  );
};

export default Analytics;
