import React from "react";
import Image from '../assets/carousel.jpg';

const LifestyleQuote = () => {
  return (
    <div
      className="flex items-center justify-center h-full w-full bg-cover bg-center"
      style={{ backgroundImage: `url(${Image})` }}
    >
      <div className="w-1/2 max-w-lg p-4 bg-white bg-opacity-80 rounded-md shadow-lg">
        <p className="text-lg md:text-xl lg:text-lg leading-snug mb-4">
          "Hi, I'm Patricia, I've gone from being a waitress struggling to pay the bills to secretary, to podcast host and finally a lifestyle coach who believes that everyone has the potential to live their best life. My approach is compassionate and supportive, and I work with my clients to develop the mindset and habits necessary to achieve their goals and overcome the obstacles that may be holding them back.
          My passion for elevating people and helping them strive is at the heart of everything I do. Whether it's helping someone cultivate greater self-awareness, develop a more positive mindset, or achieve a specific goal, I am dedicated to empowering my clients and helping them reach their full potential.
          If you're ready to take your life to the next level and unlock your true potential, don't hesitate to reach out to me today. Let's team up and embark on a transformational journey towards the best version of yourself!"
        </p>
        <p className="text-sm md:text-base">Patricia M.</p>
      </div>
    </div>
  );
};

export default LifestyleQuote;
