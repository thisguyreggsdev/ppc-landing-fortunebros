import { CheckCircleIcon } from '@heroicons/react/20/solid';

export default function Example() {
  const reloadPage = () => {
    window.location.reload();
  };

  setTimeout(() => {
    reloadPage();
  }, 2000); // delay in milliseconds

  return (
    <div className="flex flex-col items-center justify-center">
      <div className="rounded-md bg-green-50 p-2 w-100 mt-8">
        <div className="flex">
          <div className="flex-shrink-0">
            <CheckCircleIcon className="h-5 w-5 text-green-400" aria-hidden="true" />
          </div>
          <div className="ml-8">
            <p className="text-lg font-medium text-green-800">Successfully sent! Our representatives will be in touch with you soon</p>
          </div>
        </div>
      </div>
      <div className="h-20"></div>
    </div>
  );
}
