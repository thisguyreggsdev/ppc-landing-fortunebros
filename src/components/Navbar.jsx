import React, { useState } from 'react';
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai';
import Logo from '../assets/fit.png';

const Navbar = () => {
  const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const [nav, setNav] = useState(false);

  const handleNav = () => {
    setNav(!nav);
  };

  return (
    <div className='flex justify-between items-center h-24 max-w-[1240px] mx-auto px-4 text-white'>
     <img src={Logo} alt="FortuneBros" className='h-40 w-40 mt-20' />
      <ul className={`hidden md:flex justify-center ${nav ? 'absolute top-0 left-0 w-full h-screen bg-gray-800 z-20 flex-col items-center' : ''}`}>
        <li className='px-4 py-2 text-lg font-medium text-white cursor-pointer transition-all duration-200 transform hover:text-white hover:scale-105' onClick={handleClickScroll('hero')}>
          Home
        </li>
        <li className='px-4 py-2 text-lg font-medium text-white cursor-pointer transition-all duration-300 transform hover:text-white hover:scale-105' onClick={handleClickScroll('testimonials')}>
          About
        </li>
        <li className='px-4 py-2 text-lg font-medium text-white cursor-pointer transition-all duration-300 transform hover:text-white hover:scale-105' onClick={handleClickScroll('form-container')}>
          Contact
        </li>
      </ul>
      <div onClick={handleNav} className='block md:hidden text-white'>
          {nav ? <AiOutlineClose size={20}/> : <AiOutlineMenu size={20} />}
      </div>
      <ul className={nav ? 'fixed left-0 top-0 w-[60%] h-full border-r border-r-gray-900 bg-[#000300] ease-in-out duration-500' : 'ease-in-out duration-500 fixed left-[-100%]'}>
          <li className='px-4 py-2 text-lg font-medium text-gray-300 cursor-pointer transition-all duration-200 transform hover:text-white hover:scale-105' onClick={handleClickScroll('hero')}>
            Home
          </li>
          <li className='px-4 py-2 text-lg font-medium text-gray-300 cursor-pointer transition-all duration-300 transform hover:text-white hover:scale-105' onClick={handleClickScroll('testimonials')}>
            About
          </li>
          <li className='px-4 py-2 text-lg font-medium text-gray-300 cursor-pointer transition-all duration-300 transform hover:text-white hover:scale-105' onClick={handleClickScroll('form-container')}>
            Contact
          </li>
        </ul>
        </div>
      )};

export default Navbar;
