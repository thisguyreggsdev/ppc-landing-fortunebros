export default function Example() {
  return (
    <section className="bg-gray-800 py-24 sm:py-32">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto grid max-w-2xl grid-cols-1 lg:mx-0 lg:max-w-none lg:grid-cols-2">
          <div className="flex flex-col pb-10 sm:pb-16 lg:pb-0 lg:pr-8 xl:pr-20">
            <figure className="mt-10 flex flex-auto flex-col justify-between">
              <blockquote className="text-lg leading-8 text-white">
                <p className="text-justify">
                  Aldrin is a former marine who brings his discipline and precision to the art of barbering. He has honed his skills through years of training and practice, 
                  and he is known for his mastery of the latest trends in men's grooming. 
                  Aldrin is particularly skilled in creating precision fades and beard grooming techniques, 
                  which are in high demand among his clients. With his friendly demeanor and professional approach,
                   Aldrin has earned the trust of his clients, who appreciate his ability to help them look and feel their best. 
                   Whether you are looking for a classic cut or a modern style, 
                  Aldrin has the expertise to deliver exceptional results every time.
                </p>
              </blockquote>
              <figcaption className="mt-10 flex items-center gap-x-6">
                <img
                  className="h-14 w-14 rounded-full bg-gray-800"
                  src="https://s3-media0.fl.yelpcdn.com/bphoto/10jvOHcbu4L3SpnKd2PiXA/300s.jpg"
                  alt=""
                />
                <div className="text-base">
                  <div className="font-semibold text-white">Aldrin Moore</div>
                  <div className="mt-1 text-gray-400">Cut, Dapper, & Shave</div>
                </div>
              </figcaption>
            </figure>
          </div>
          <div className="flex flex-col border-t border-white/10 pt-10 sm:pt-16 lg:border-l lg:border-t-0 lg:pl-8 lg:pt-0 xl:pl-20">
            <figure className="mt-10 flex flex-auto flex-col justify-between">
              <blockquote className="text-lg leading-8 text-white">
                <p className="text-justify">
                    Adrian is a true master of his craft, with over a decade of experience in the industry.
                    Specializing in classic cuts and straight razor shaves, Adrian is known for his attention to detail and commitment to excellence.
                    He takes the time to get to know his clients, and he always provides personalized recommendations to help them achieve their desired look. 
                    With his warm personality and unmatched expertise, He is known for his ability to blend traditional techniques with modern flair, creating unique looks that stand out from the crowd.
                    Adrian has built a loyal following of clients who trust him with their hair and grooming needs.
                </p>
              </blockquote>
              <figcaption className="mt-10 flex items-center gap-x-6">
                <img
                  className="h-14 w-14 rounded-full bg-gray-800"
                  src="https://s3-media0.fl.yelpcdn.com/bphoto/tTU6wGHVAXvkFRaHBm2llQ/348s.jpg"
                  alt=""
                />
                <div className="text-base">
                  <div className="font-semibold text-white">Adrian Swinley</div>
                  <div className="mt-1 text-gray-400">Cut & Groom</div>
                </div>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </section>
  )
}
