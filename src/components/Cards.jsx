import React from 'react';
import Single from '../assets/single.png'
import Double from '../assets/double.png'
import Triple from '../assets/triple.png'

const Cards = () => {
    const handleClickScroll = () => {
    const formContainer = document.getElementById('form-container');
    if (formContainer) {
      formContainer.scrollIntoView({ behavior: 'smooth' });
    }
  };


  return (
    <div className='w-full py-[10rem] px-4 bg-white'>
      <div className='max-w-[1240px] mx-auto grid md:grid-cols-3 gap-8'>
          <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
              <img className='w-20 mx-auto mt-[-3rem] bg-white' src={Single} alt="/" />
              <h2 className='text-2xl font-bold text-center py-8'>Cut & Trim</h2>
              <p className='text-center text-4xl font-bold'>$22</p>
              <div className='text-center font-medium'>
              </div>
              <button className='bg-[#00df9a] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3'onClick={handleClickScroll}>Book appointment</button>
          </div>
          <div className='w-full shadow-xl bg-gray-100 flex flex-col p-4 md:my-0 my-8 rounded-lg hover:scale-105 duration-300'>
              <img className='w-20 mx-auto mt-[-3rem] bg-transparent' src={Double} alt="/" />
              <h2 className='text-2xl font-bold text-center py-8'>Shave</h2>
              <p className='text-center text-4xl font-bold'>$12</p>
              <div className='text-center font-medium'>
              </div>
              <button className='bg-black text-[#00df9a] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3'onClick={handleClickScroll}>Book appointment</button>
          </div>
          <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
              <img className='w-20 mx-auto mt-[-3rem] bg-white' src={Triple} alt="/" />
              <h2 className='text-2xl font-bold text-center py-8'>Cut & Shave</h2>
              <p className='text-center text-4xl font-bold'>$32</p>
              <div className='text-center font-medium'>
              </div>
              <button className='bg-yellow-500 w-[200px] rounded-md font-medium text-white my-6 mx-auto px-6 py-3' onClick={handleClickScroll} >Book appoinment</button>
          </div>
      </div>
    </div>
  );
};

export default Cards;
