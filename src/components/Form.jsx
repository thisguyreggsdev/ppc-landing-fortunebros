import React, {useState} from 'react';
import { useForm, ValidationError } from '@formspree/react';
import ConfirmButton from './Button';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default function Form() {
  const [selectedDate, setSelectedDate] = useState(null);
  const [state, handleSubmit] = useForm("xknaylaj");
      if (state.succeeded) {
   return <ConfirmButton/>
   

  }
   

  return (
    <div className="relative isolate bg-gray-800 px-6 py-24 sm:py-32 lg:px-8">
      <div className="mx-auto max-w-xl lg:max-w-4xl">
        <h2 className="text-4xl font-bold tracking-tight text-white">Live comfortably <span class="text-yellow-600">in style</span></h2>
        <p className="mt-2 text-lg leading-8 text-white">
          Get your appointments booked now.
        </p>
        <div className="mt-16 flex flex-col gap-16 sm:gap-y-20 lg:flex-row">
          <form onSubmit={handleSubmit} className="lg:flex-auto">
            <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
              <div>
                <label htmlFor="first-name" className="block text-sm font-semibold leading-6 text-white">
                  First name
                </label>
                <div className="mt-2.5">
                  <input
                    type="text"
                    name="first-name"
                    id="first-name"
                    autoComplete="given-name"
                    className="block w-full rounded-md border-0 px-3.5 py-2 text-yellow-900 shadow-sm ring-1 ring-inset ring-yellow-300 placeholder:text-yellow-400 focus:ring-2 focus:ring-inset focus:ring-yellow-600 sm:text-sm sm:leading-6"
                  />
                </div>
              </div>
              <div>
              <label htmlFor="last-name" className="block text-sm font-semibold leading-6 text-white">
                  Last name
                </label>
                <div className="mt-2.5">
                  <input
                    type="text"
                    name="last-name"
                    id="last-name"
                    autoComplete="family-name"
                    className="block w-full rounded-md border-0 px-3.5 py-2 text-yellow-900 shadow-sm ring-1 ring-inset ring-yellow-300 placeholder:text-yellow-400 focus:ring-2 focus:ring-inset focus:ring-yellow-600 sm:text-sm sm:leading-6"
                    />
                  </div>
              </div>
              <div className="sm:col-span-2">
            <label htmlFor="email" className="block text-sm font-semibold leading-6 text-white">
              Email address
              </label>
              <div className="mt-2.5">
                  <input
                    type="email"
                    name="email"
                    id="email"
                    autoComplete="email"
                    className="block w-full rounded-md border-0 px-3.5 py-2 text-yellow-900 shadow-sm ring-1 ring-inset ring-yellow-300 placeholder:text-yellow-400 focus:ring-2 focus:ring-inset focus:ring-yellow-600 sm:text-sm sm:leading-6"
                    required
                    />
              <ValidationError 
                    prefix="Email" 
                    field="email" 
                    errors={state.errors} 
                    />
                    </div>
                </div>
                 <div className="mt-5">
                <label htmlFor="services" className="block text-sm font-semibold leading-6 text-white">
                  Services
                </label>
                <div className="mt-2.5">
                  <select
                    name="services"
                    id="services"
                    required
                    className="block w-full rounded-md border-0 px-3.5 py-2 text-yellow-900 shadow-sm ring-1 ring-inset ring-yellow-300 placeholder:text-yellow-400 focus:ring-2 focus:ring-inset focus:ring-yellow-600 sm:text-sm sm:leading-6"
                  >
                    <option value="others">Please select your preferred services:</option>
                    <option value="detailing">Cut</option>
                    <option value="clean&buff">Shave</option>
                    <option value="inspection">Cut & Shave</option>
                    <option value="others">Others specify in messsage</option>
                  </select>
                </div>
              </div>
               <div>
                <div className="mt-5">
                  <label htmlFor="appointment" className="block text-sm font-semibold leading-6 text-white">
                    Date of appointment
                  </label>
                  <div className="mt-2.5">
                    <DatePicker
                      name="appointment"
                      id="appointment"
                      selected={selectedDate}
                      onChange={(date) => setSelectedDate(date)}
                      minDate={new Date()}
                      required
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-yellow-900 shadow-sm ring-1 ring-inset ring-yellow-300 placeholder:text-yellow-400 focus:ring-2 focus:ring-inset focus:ring-yellow-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
              </div>
              <div className="sm:col-span-2">
              <label htmlFor="message" className="block text-sm font-semibold leading-6 text-white">
              Message
              </label>
              <div className="mt-2.5">
              <textarea
                    name="message"
                    id="message"
                    rows={4}
                    className="block w-full rounded-md border-0 px-3.5 py-2 text-yellow-900 shadow-sm ring-1 ring-inset ring-yellow-300 placeholder:text-yellow-400 focus:ring-2 focus:ring-inset focus:ring-yellow-600 sm:text-sm sm:leading-6"
                            required
                            />
              <ValidationError
                  prefix="Message" 
                  field="message" 
                  errors={state.errors} />
              
                  </div>
               </div>
              <div className="sm:col-span-2">
              <button
                  type="submit"
                  disabled={state.submitting}
                  className="inline-flex items-center justify-center w-full rounded-md border border-transparent px-5 py-3 bg-yellow-600 text-base font-medium text-white shadow-sm hover:bg-yellow-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-yellow-400 sm:text-sm disabled:opacity-50 disabled:cursor-not-allowed">
                  {state.submitting ? "Submitting..." : "Submit"}
              </button>
            </div>
          </div>
          </form>
        </div>
      </div>
      </div>
      );
    }

