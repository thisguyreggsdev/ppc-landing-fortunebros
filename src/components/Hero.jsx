import React from 'react';
import Typed from 'react-typed';
import imageSrc from '../assets/hero.jpg';

const Hero = () => {
  const handleClickScroll = () => {
    const formContainer = document.getElementById('form-container');
    if (formContainer) {
      formContainer.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const backgroundImageStyle = {
    backgroundImage: `url(${imageSrc})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  };

  return (
    <div className='text-white' style={backgroundImageStyle}>
      <div className='max-w-[800px] mt-[-96px] w-full h-screen mx-auto text-center flex flex-col justify-center'>
        <div>
          <div className='flex justify-center items-center'>
            <p className='md:text-5xl sm:text-4xl text-xl font-bold py-4 text-white'>
              The place to be for a dapper look.
            </p>
          </div>
         
          <button className='text-white w-[200px] rounded-md font-medium my-6 mx-auto md:mx-0 py-3 border-2  bg-slate-500 hover:bg-white hover:text-gray-900 transition duration-300 ease-in-out' onClick={handleClickScroll}>Book Now</button>

        </div>
      </div>
    </div>
  );
}

export default Hero;
