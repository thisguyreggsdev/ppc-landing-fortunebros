import React, { useState } from 'react';
import Logo2 from '../assets/fit2.png';

const PrivacyPolicyPopup = () => {
  const [showPopup, setShowPopup] = useState(false);

  const handleShowPopup = () => {
    setShowPopup(true);
  };

  const handleClosePopup = () => {
    setShowPopup(false);
  };

  return (
    <div>
      <button onClick={handleShowPopup} className='text-yellow-600 font-bold'>
        Privacy Policy
      </button>
      {showPopup && (
        <div className='fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50 flex items-center justify-center'>
          <div className='bg-gray-500 p-8 max-w-lg rounded-lg'>
            <h3 className='text-2xl text-yellow-500 font-bold leading-tight'>
              Privacy Policy
            </h3>
            <p className='py-4 text-white'>
              By using our site, you acknowledge that you have read and understand our Privacy Policy, which outlines how we collect, use, and protect your personal information, including but not limited to your name, email address, and other identifying information. We take your privacy seriously and we are committed to protecting your personal information in accordance with applicable laws and regulations. Our Privacy Policy describes how we collect, use, store, and disclose your personal information, as well as your rights and choices in relation to your personal information.

              Additionally, our Privacy Policy also outlines our use of cookies, which are small text files that are placed on your device when you access our website. Cookies enable us to enhance your user experience by remembering your preferences and providing personalized content. We may use both session cookies, which expire when you close your browser, and persistent cookies, which remain on your device until they expire or are deleted. We may also use third-party cookies, which are cookies set by a domain other than our website.

              We believe in transparency and we want you to have a clear understanding of how we collect, use, and protect your personal information. Therefore, we encourage you to read our Privacy Policy carefully before using our site. If you have any questions or concerns about our Privacy Policy or our use of cookies, please do not hesitate to contact us.
            </p>
            <button className='text-red-600' onClick={handleClosePopup}>Close
            </button>

          </div>
        </div>
      )}
    </div>
  );
};

const CookiePolicyPopup = () => {
  const [showPopup, setShowPopup] = useState(false);

  const handleShowPopup = () => {
    setShowPopup(true);
  };

  const handleClosePopup = () => {
    setShowPopup(false);
  };

  return (
    <div>
      <button onClick={handleShowPopup} className='text-yellow-600 font-bold'>
        Cookie Policy
      </button>
      {showPopup && (
        <div className='fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50 flex items-center justify-center'>
          <div className='bg-gray-500 p-8 max-w-lg rounded-lg'>
            <h3 className='text-2xl text-yellow-500 font-bold leading-tight'>
              Cookie Policy
            </h3>
            <p className='py-4 text-white'>
              At Fortune Bros, we are committed to providing the best possible user experience on our website. We use cookies to help us achieve this goal by tracking your interactions with our site and personalizing your experience based on your preferences. Cookies are small text files that are stored on your device when you visit our website. These files allow us to remember your preferences, login credentials, and browsing history so that we can provide you with personalized content and a seamless user experience. By using our website, you consent to the use of cookies as outlined in our Cookie Policy. Rest assured that we take your privacy seriously and do not collect any personally identifiable information through our use of cookies.
            </p>
            <button className='text-red-600' onClick={handleClosePopup}>Close</button>
          </div>
        </div>
      )}
    </div>
  );
};

const Footer = () => {

  return (
    <div className='max-w-[1100px] mx-auto py-16 px-4 flex items-center justify-between text-gray-300'>
      <div className='flex items-center'>
        <img src={Logo2} alt='Logo' height='120' width='120' />
      </div>
      <div className='flex space-x4'>
        <PrivacyPolicyPopup />
        <div className='ml-4'>
          <CookiePolicyPopup />
        </div>
      </div>
      <div className='flex flex-col items-start ml-8'>
        <p className='mb-2 text-gray-900'>
          525 Rue Saint-Joseph E, Québec, QC G1K 3B7, Canada
        </p>
        <p className='text-gray-900'>
          © 2023 FORTUNE BROS All rights reserved.
        </p>
        <p>📞 +1-885-884-8855</p>
        <p>📨 FortuneBros@fb.ca</p>
      </div>
    </div>

  );
};


export default Footer;
