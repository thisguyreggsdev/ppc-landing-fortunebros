import React from 'react';
import Analytics from './components/Analytics';
import Cards from './components/Cards';
import Footer from './components/Footer';
import Hero from './components/Hero';
import Navbar from './components/Navbar';
import Form from './components/Form';
import Testimonials from './components/Testimonials';




function App() {
  return (
    <div>
      <Navbar />
      <div id="hero">
      <Hero />
      </div>
      <div id="analytics">
      <Analytics />
      </div>
      <div id="testimonials">
      <Testimonials/>
      </div>
      <Cards />
      <div id="form-container">
      <Form/>
      </div>
      <Footer />
    </div>
  );
}

export default App;
